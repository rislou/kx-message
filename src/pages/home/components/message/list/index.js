import {withStyles} from '@material-ui/core/styles'

import List from './component'
import styles from './styles'

export default withStyles(styles)(List)
