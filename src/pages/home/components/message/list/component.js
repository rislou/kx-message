import {arrayOf, bool, number, objectOf, oneOfType, shape, string} from 'prop-types'
import {Table, TableBody, TableCell, TableHead, TableRow} from '@material-ui/core'
import Check from '@material-ui/icons/Check'
import React, {Component} from 'react'

import Card from 'common/components/card'

import wording from './config/wording'

export default class List extends Component {
  static propTypes = {
    classes: objectOf(string),
    messages: arrayOf(
      shape({
        id: number,
        content: string,
        isPublic: oneOfType([bool, string]),
      }),
    ),
  }

  static defaultProps = {
    classes: {},
    messages: [],
  }

  renderMessageList = () => {
    const {classes, messages} = this.props

    return (
      <Table className={classes.table}>
        <TableHead color="primary">
          <TableRow>
            <TableCell>{wording.table.columns.content}</TableCell>
            <TableCell align="right">{wording.table.columns.isPublic}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {messages.map(message => (
            <TableRow key={message.id}>
              <TableCell component="th" scope="row">
                {message.content}
              </TableCell>
              <TableCell align="right">{message.isPublic && <Check color="primary" />}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    )
  }

  render() {
    return <Card title={wording.card.title} content={this.renderMessageList()} />
  }
}
