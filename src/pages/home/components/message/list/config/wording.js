export default {
  card: {
    title: 'List of messages',
  },
  table: {
    columns: {
      content: 'Content',
      isPublic: 'Public',
    },
  },
}
