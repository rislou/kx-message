export default {
  card: {
    title: 'Compose a new message',
  },
  form: {
    labels: {
      content: 'What do you want to write ?',
      isPublic: 'Yes, this message is public',
      submitButton: 'Write down',
    },
    feedback: {
      success: 'Message send el captain !',
      error: 'Oups, feels like there is some trouble in paradise...:-)',
    },
  },
}
