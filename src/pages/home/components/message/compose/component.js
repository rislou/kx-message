import {func, objectOf, string} from 'prop-types'

import React, {Component} from 'react'

import Button from '@material-ui/core/es/Button/Button'

import Checkbox from '@material-ui/core/es/Checkbox/Checkbox'
import InputLabel from '@material-ui/core/es/InputLabel/InputLabel'

import Input from '@material-ui/core/es/Input/Input'

import FormControl from '@material-ui/core/es/FormControl/FormControl'
import FormControlLabel from '@material-ui/core/es/FormControlLabel/FormControlLabel'

import Card from 'common/components/card'

import wording from './config/wording'

export default class Compose extends Component {
  static propTypes = {
    classes: objectOf(string),
    handleSubmit: func,
    notification: string,
  }

  static defaultProps = {
    classes: {},
    handleSubmit: Function.prototype,
    notification: '',
  }

  constructor(props) {
    super(props)
    this.state = {
      content: '',
      isPublic: false,
    }
  }

  handleChange = (field, name) => event => {
    this.setState({[name]: event.target[field]})
  }

  handleOnClick = () => {
    const {handleSubmit} = this.props
    const {content, isPublic} = this.state

    handleSubmit(content, isPublic)

    this.setState({
      content: '',
      isPublic: false,
    })
  }

  renderForm = () => {
    const {classes} = this.props
    const {content, isPublic} = this.state

    return (
      <form>
        <FormControl margin="normal" required fullWidth>
          <InputLabel htmlFor="content" shrink>
            {wording.form.labels.content}
          </InputLabel>
          <Input id="content" name="content" autoFocus value={content} onChange={this.handleChange('value', 'content')} />
        </FormControl>
        <FormControl margin="normal" required fullWidth>
          <FormControlLabel
            control={<Checkbox checked={isPublic} onChange={this.handleChange('checked', 'isPublic')} color="primary" />}
            label={wording.form.labels.isPublic}
          />
        </FormControl>
        <Button variant="outlined" color="primary" className={classes.button} onClick={this.handleOnClick} disabled={!content} fullWidth>
          {wording.form.labels.submitButton}
        </Button>
      </form>
    )
  }

  render() {
    return <Card title={wording.card.title} content={this.renderForm()} />
  }
}
