import {withStyles} from '@material-ui/core/styles'

import Compose from './component'
import styles from './styles'

export default withStyles(styles)(Compose)
