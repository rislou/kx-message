import React, {Component} from 'react'

import {objectOf, string} from 'prop-types'
import Grid from '@material-ui/core/Grid'

import AppBar from '@material-ui/core/es/AppBar/AppBar'

import Toolbar from '@material-ui/core/es/Toolbar/Toolbar'

import axios from 'axios'

import MessageCompose from 'pages/home/components/message/compose'

import MessageList from 'pages/home/components/message/list'

export default class Home extends Component {
  static propTypes = {
    classes: objectOf(string),
  }

  static defaultProps = {
    classes: {},
  }

  constructor(props) {
    super(props)
    this.state = {
      messages: [],
    }
  }

  componentWillMount() {
    axios
      .get('data/messagesMock.json')
      .then(({data}) => {
        this.setState({messages: data})
      })
      .catch()
  }

  handleSubmit = (content, isPublic) => {
    this.setState(prevState => {
      return {
        messages: [...prevState.messages, {id: +new Date(), content, isPublic}],
      }
    })
  }

  render() {
    const {classes} = this.props
    const {messages} = this.state

    return (
      <div className={classes.root}>
        <Grid container spacing={24}>
          <AppBar position="static" color="default">
            <Toolbar>
              <img src="images/logo.svg" alt="Kicklox" width="15%" />
            </Toolbar>
          </AppBar>
          <Grid item xs={8}>
            <MessageList messages={messages} />
          </Grid>
          <Grid item xs={4}>
            <MessageCompose handleSubmit={this.handleSubmit} />
          </Grid>
        </Grid>
      </div>
    )
  }
}
