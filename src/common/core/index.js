import React from 'react'

import {MuiThemeProvider} from '@material-ui/core/styles'

import Home from 'pages/home/component'
import theme from '../theme'

const Core = () => (
  <MuiThemeProvider theme={theme}>
    <Home />
  </MuiThemeProvider>
)

export default Core
