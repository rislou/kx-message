import {any, objectOf, string} from 'prop-types'
import CardContent from '@material-ui/core/es/CardContent/CardContent'
import MuiCard from '@material-ui/core/es/Card/Card'
import React, {PureComponent} from 'react'
import Typography from '@material-ui/core/es/Typography/Typography'

export default class Card extends PureComponent {
  static propTypes = {
    classes: objectOf(string),
    content: objectOf(any),
    title: string,
  }

  static defaultProps = {
    classes: {},
    content: Function.prototype,
    title: '',
  }

  render() {
    const {classes, title, content} = this.props

    return (
      <MuiCard className={classes.card}>
        <CardContent>
          <Typography className={classes.title} variant="subtitle1" gutterBottom>
            {title}
          </Typography>
          {content}
        </CardContent>
      </MuiCard>
    )
  }
}
