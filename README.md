# Kx message

## Stack

- React.js (Node, Yarn) with CRA

## Requirements

- nodejs : https://nodejs.org/en/download/
- yarn : https://yarnpkg.com/lang/en/docs/install

## Install

`yarn`

## Post install

`cp .env.dist .env`

```dotenv
NODE_PATH=src
```

## Run

`yarn start`
